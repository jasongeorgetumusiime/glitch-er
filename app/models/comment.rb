class Comment < ApplicationRecord
  belongs_to :user
  belongs_to :entry

  after_create { |record| CommentActivity.write(record) }
end
