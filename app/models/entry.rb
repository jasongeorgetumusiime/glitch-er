class Entry < ApplicationRecord
  belongs_to :feed
  has_many :comments
end
