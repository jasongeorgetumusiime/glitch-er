class Vote < ApplicationRecord
  belongs_to :user
end

class UpVote < Vote
  belongs_to :entry, conter_cache: true
  after_create {|record | VoteActivity.write(record)}
end

class DownVote < Vote
  belongs_to :entry, conter_cache: true
end