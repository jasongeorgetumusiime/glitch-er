class CreateEntries < ActiveRecord::Migration[6.0]
  def change
    create_table :entries do |t|
      t.references :feed
      t.string :title
      t.string :url
      t.text :content
      t.string :title
      t.datetime :published_date
      t.integer :up_vote_count
      t.integer :down_vote_count
      t.integer :comments_count

      t.timestamps
    end
  end
end
