class UpdateActivitiesTable < ActiveRecord::Migration[6.0]
  def change
    add_reference :activities, :user, foreign_key: true
    add_column :activities, :type, :string
    add_reference :activities, :feed, foreign_key: true
    add_column :activities, :followed_user_id, :integer
    add_column :activities, :entry_id, :integer
    add_column :activities, :content, :text
    add_column :activities, :following_user_id, :integer
  end
end
